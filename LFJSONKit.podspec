Pod::Spec.new do |spec|
  spec.name         = 'LFJSONKit'
  spec.version      = '1.0'
  spec.license      = 'MIT'
  spec.summary      = 'LFJSONKit'
  spec.homepage     = 'https://nvidalgt@bitbucket.org/globaltask/lfjsonkit'
  spec.author       = 'Adrian Claret'
  spec.source       = { :git => 'https://nvidalgt@bitbucket.org/globaltask/lfjsonkit.git' }
  spec.source_files = 'Library/*'
  spec.requires_arc = true
end